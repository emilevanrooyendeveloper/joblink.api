﻿using JobLink.Core.Data;
using JobLink.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JobLink.Api.ADO.Repositories
{
    public interface IInvoiceItemRepositoryAsync : IInvoiceItemRepository
    {
        Task<ICollection<ICustomerInvoiceItem>> GetCustomerInvoiceItemsAsync(Guid invoiceID);
        Task<ICustomerInvoiceItem> InsertAsync(ICustomerInvoiceItem customerInvoiceItem);
        Task RemoveAsync(Guid itemID);
    }

    public class InvoiceItemRepositoryAsync : InvoiceItemRepository, IInvoiceItemRepositoryAsync
    {
        public InvoiceItemRepositoryAsync()
        { }

        public InvoiceItemRepositoryAsync(IDatabase database)
            : base(database)
        { }

        public async Task<ICollection<ICustomerInvoiceItem>> GetCustomerInvoiceItemsAsync(Guid invoiceID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_InvoiceItems_InvoiceID]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@invoiceID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = invoiceID
                });
                // input parameters that the stored proc will be expecting

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                {
                    return ReadCollection(datareader, this);
                }
            }
        }

        public async Task<ICustomerInvoiceItem> InsertAsync(ICustomerInvoiceItem customerInvoiceItem)
        {
            Database.Validate();
            customerInvoiceItem.ID = Guid.NewGuid();
            using (var command = new SqlCommand("[test].[USP_INSERT_InvoiceItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoiceItem.ID
                });
                command.Parameters.Add(new SqlParameter("@invoiceID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoiceItem.InvoiceID
                });
                command.Parameters.Add(new SqlParameter("@quantity", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoiceItem.Quantity
                });
                command.Parameters.Add(new SqlParameter("@inventoryItemID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoiceItem.InventoryItemID
                });


                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                { }

                return customerInvoiceItem;
            }
        }

        public async Task RemoveAsync(Guid itemID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_UPDATE_InvoiceItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = itemID
                });


                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                { }
            }
        }
    }
    }
