﻿using JobLink.Core.Data;
using JobLink.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JobLink.Api.ADO.Repositories
{
    public interface IInventoryItemRepositoryAsync : IInventoryItemRepository
    {
        Task<ICollection<IInventoryItem>> GetInventoryItemsAsync();
        Task<IInventoryItem> GetInventoryItemAsync(Guid inventoryItemID);
        Task<IInventoryItem> InsertInventoryItemAsync(IInventoryItem inventoryItem);
        Task<IInventoryItem> UpdateInventoryItemAsync(IInventoryItem inventoryItem);
    }

    public class InventoryItemRepositoryAsync : InventoryItemRepository, IInventoryItemRepositoryAsync
    {
        public InventoryItemRepositoryAsync()
        { }

        public InventoryItemRepositoryAsync(IDatabase database)
            : base(database)
        { }

        public async Task<IInventoryItem> GetInventoryItemAsync(Guid inventoryItemID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_InventoryItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItemID
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                {
                    return await ReadAsync(datareader, this);
                }
            }
        }

        public async Task<ICollection<IInventoryItem>> GetInventoryItemsAsync()
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_InventoryItems]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                {
                    return await ReadCollectionAsync(datareader, this);
                }
            }
        }

        public async Task<IInventoryItem> InsertInventoryItemAsync(IInventoryItem inventoryItem)
        {
            Database.Validate();
            inventoryItem.ID = Guid.NewGuid();
            using (var command = new SqlCommand("[test].[USP_INSERT_InventoryItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.ID
                });
                command.Parameters.Add(new SqlParameter("@itemDescription", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.ItemDescription
                });
                command.Parameters.Add(new SqlParameter("@unitPrice", SqlDbType.Decimal)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.UnitPrice
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                { }

                return inventoryItem;
            }
        }

        public async Task<IInventoryItem> UpdateInventoryItemAsync(IInventoryItem inventoryItem)
        {
            Database.Validate();

            using (var command = new SqlCommand("[test].[USP_UPDATE_InventoryItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.ID
                });
                command.Parameters.Add(new SqlParameter("@itemDescription", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.ItemDescription
                });
                command.Parameters.Add(new SqlParameter("@unitPrice", SqlDbType.Decimal)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.UnitPrice
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                { }

                return inventoryItem;
            }
        }
    }

    }
