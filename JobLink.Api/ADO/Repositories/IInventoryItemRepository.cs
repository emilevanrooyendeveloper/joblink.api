﻿using JobLink.Core.Data;
using JobLink.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace JobLink.Api.ADO.Repositories
{
    public interface IInventoryItemRepository :
        IAdaptor<System.Data.IDataReader, IInventoryItem>,
        System.IDisposable
    {
        ICollection<IInventoryItem> GetInventoryItems();
        IInventoryItem GetInventoryItem(Guid inventoryItemID);
        IInventoryItem InsertInventoryItem(IInventoryItem inventoryItem);
        IInventoryItem UpdateInventoryItem(IInventoryItem inventoryItem);
    }

    public class InventoryItemRepository : RepositoryBase, IInventoryItemRepository
    {
        public InventoryItemRepository()
        { }


        public InventoryItemRepository(IDatabase database)
            : base(database)
        { }

        public IInventoryItem Adapt(IDataReader datareader)
        {
            return new InventoryItem()
            {
                ID = (Guid)datareader["ID"],
                ItemDescription = datareader["ItemDescription"].ToString(),
                UnitPrice = (decimal)datareader["UnitPrice"],
                IsDeleted = (bool)datareader["IsDeleted"]
            };
        }

        public IInventoryItem GetInventoryItem(Guid inventoryItemID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_InventoryItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItemID
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                {
                    return Read(datareader, this);
                }
            }
        }

        public ICollection<IInventoryItem> GetInventoryItems()
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_InventoryItems]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                {
                    return ReadCollection(datareader, this);
                }
            }
        }

        public IInventoryItem InsertInventoryItem(IInventoryItem inventoryItem)
        {
            Database.Validate();
            inventoryItem.ID = Guid.NewGuid();
            using (var command = new SqlCommand("[test].[USP_INSERT_InventoryItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.ID
                });
                command.Parameters.Add(new SqlParameter("@itemDescription", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.ItemDescription
                });
                command.Parameters.Add(new SqlParameter("@unitPrice", SqlDbType.Decimal)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.UnitPrice
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                { }

                return inventoryItem;
            }
        }

        public IInventoryItem UpdateInventoryItem(IInventoryItem inventoryItem)
        {
            Database.Validate();

            using (var command = new SqlCommand("[test].[USP_UPDATE_InventoryItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.ID
                });
                command.Parameters.Add(new SqlParameter("@itemDescription", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.ItemDescription
                });
                command.Parameters.Add(new SqlParameter("@unitPrice", SqlDbType.Decimal)
                {
                    Direction = ParameterDirection.Input,
                    Value = inventoryItem.UnitPrice
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                { }

                return inventoryItem;
            }
        }
    }

}
