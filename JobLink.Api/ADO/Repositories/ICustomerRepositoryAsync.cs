﻿using JobLink.Core.Data;
using JobLink.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JobLink.ADO.Repositories
{
    public interface ICustomerRepositoryAsync : ICustomerRepository
    {
        Task<ICollection<ICustomer>> GetCustomersAsync();
        Task<ICustomer> GetCustomerAsync(Guid customerID);
        Task<ICustomer> InsertCustomerAsync(ICustomer customer);
        Task<ICustomer> UpdateCustomerAsync(ICustomer customer);
    }

    public class CustomerRepositoryAsync : CustomerRepository, ICustomerRepositoryAsync
    {
        public CustomerRepositoryAsync()
        { }

        public CustomerRepositoryAsync(IDatabase database)
            : base(database)
        { }

        public async Task<ICustomer> GetCustomerAsync(Guid customerID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_Customer_ID]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@customerID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerID
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                {
                    return Read(datareader, this);
                }
            }
        }

        public async Task<ICollection<ICustomer>> GetCustomersAsync()
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_Customers]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                {
                    return await ReadCollectionAsync(datareader, this);
                }
            }
        }

        public async Task<ICustomer> InsertCustomerAsync(ICustomer customer)
        {
            Database.Validate();
            customer.ID = Guid.NewGuid();
            using (var command = new SqlCommand("[test].[USP_INSERT_Customer]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.ID
                });
                command.Parameters.Add(new SqlParameter("@customerName", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.CustomerName
                });
                command.Parameters.Add(new SqlParameter("@customerAddress", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.Address
                });
                command.Parameters.Add(new SqlParameter("@telephoneNumber", SqlDbType.NVarChar, 15)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.TelephoneNumber
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                { }

                return customer;
            }
        }

        public async Task<ICustomer> UpdateCustomerAsync(ICustomer customer)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_UPDATE_Customer]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.ID
                });
                command.Parameters.Add(new SqlParameter("@customerName", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.CustomerName
                });
                command.Parameters.Add(new SqlParameter("@customerAddress", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.Address
                });
                command.Parameters.Add(new SqlParameter("@telephoneNumber", SqlDbType.NVarChar, 15)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.TelephoneNumber
                });
                command.Parameters.Add(new SqlParameter("@isDeleted", SqlDbType.Bit)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.IsDeleted
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                { }

                return customer;
            }
        }
    }
}
