﻿using JobLink.Core.Data;
using JobLink.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace JobLink.Api.ADO.Repositories
{
    public interface IInvoiceItemRepository :
        IAdaptor<System.Data.IDataReader, ICustomerInvoiceItem>,
        System.IDisposable
    {
        ICollection<ICustomerInvoiceItem> GetCustomerInvoiceItems(Guid invoiceID);
        ICustomerInvoiceItem Insert(ICustomerInvoiceItem customerInvoiceItem);
        void Remove(Guid itemID);
    }

    public class InvoiceItemRepository : RepositoryBase, IInvoiceItemRepository
    {
        public InvoiceItemRepository()
        { }


        public InvoiceItemRepository(IDatabase database)
            : base(database)
        { }

        public ICustomerInvoiceItem Adapt(IDataReader datareader)
        {
            return new CustomerInvoiceItem()
            {
                ID = (Guid)datareader["ID"],
                InventoryItemID = (Guid)datareader["InventoryItemID"],
                InvoiceID = (Guid)datareader["InvoiceID"],
                ItemDescription = datareader["ItemDescription"].ToString(),
                Quantity = (int)datareader["Quantity"],
                UnitPrice = (decimal)datareader["UnitPrice"],
                IsDeleted = (bool)datareader["IsDeleted"]
            };
        }

        public ICollection<ICustomerInvoiceItem> GetCustomerInvoiceItems(Guid invoiceID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_InvoiceItems_InvoiceID]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@invoiceID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = invoiceID
                });
                // input parameters that the stored proc will be expecting

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                {
                    return ReadCollection(datareader, this);
                }
            }
        }

        public ICustomerInvoiceItem Insert(ICustomerInvoiceItem customerInvoiceItem)
        {
            Database.Validate();
            customerInvoiceItem.ID = Guid.NewGuid();
            using (var command = new SqlCommand("[test].[USP_INSERT_InvoiceItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoiceItem.ID
                });
                command.Parameters.Add(new SqlParameter("@invoiceID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoiceItem.InvoiceID
                });
                command.Parameters.Add(new SqlParameter("@quantity", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoiceItem.Quantity
                });
                command.Parameters.Add(new SqlParameter("@inventoryItemID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoiceItem.InventoryItemID
                });


                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                { }

                return customerInvoiceItem;
            }
        }

        public void Remove(Guid itemID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_UPDATE_InvoiceItem]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = itemID
                });


                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                { }
            }
        }
    }
}
