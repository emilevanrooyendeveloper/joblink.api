﻿using JobLink.Core.Data;
using JobLink.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace JobLink.ADO.Repositories
{
    public interface ICustomerRepository :
        IAdaptor<System.Data.IDataReader, ICustomer>,
        System.IDisposable
    {
        ICollection<ICustomer> GetCustomers();
        ICustomer GetCustomer(Guid customerID);
        ICustomer InsertCustomer(ICustomer customer);
        ICustomer UpdateCustomer(ICustomer customer);
    }

    public class CustomerRepository : RepositoryBase, ICustomerRepository
    {
        public CustomerRepository()
        { }


        public CustomerRepository(IDatabase database)
            : base(database)
        { }

        public ICustomer Adapt(IDataReader datareader)
        {
            return new Customer()
            {
                ID = (Guid)datareader["ID"],
                CustomerName = datareader["CustomerName"].ToString(),
                Address = datareader["Address"].ToString(),
                TelephoneNumber = datareader["TelephoneNumber"].ToString(),
                IsDeleted = (bool)datareader["IsDeleted"]
            };
        }

        public ICustomer GetCustomer(Guid customerID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_Customer_ID]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@customerID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerID
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                {
                    return Read(datareader, this);
                }
            }
        }

        public ICollection<ICustomer> GetCustomers()
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_Customers]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                {
                    return ReadCollection(datareader, this);
                }
            }
        }

        public ICustomer InsertCustomer(ICustomer customer)
        {
            Database.Validate();
            customer.ID = Guid.NewGuid();
            using (var command = new SqlCommand("[test].[USP_INSERT_Customer]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.ID
                });
                command.Parameters.Add(new SqlParameter("@customerName", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.CustomerName
                });
                command.Parameters.Add(new SqlParameter("@customerAddress", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.Address
                });
                command.Parameters.Add(new SqlParameter("@telephoneNumber", SqlDbType.NVarChar, 15)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.TelephoneNumber
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                { }

                return customer;
            }
        }

        public ICustomer UpdateCustomer(ICustomer customer)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_UPDATE_Customer]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.ID
                });
                command.Parameters.Add(new SqlParameter("@customerName", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.CustomerName
                });
                command.Parameters.Add(new SqlParameter("@customerAddress", SqlDbType.NVarChar, -1)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.Address
                });
                command.Parameters.Add(new SqlParameter("@telephoneNumber", SqlDbType.NVarChar, 15)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.TelephoneNumber
                });
                command.Parameters.Add(new SqlParameter("@isDeleted", SqlDbType.Bit)
                {
                    Direction = ParameterDirection.Input,
                    Value = customer.IsDeleted
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                { }

                return customer;
            }
        }
    }
}
