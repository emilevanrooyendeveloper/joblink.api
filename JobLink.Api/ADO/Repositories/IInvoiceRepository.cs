﻿using JobLink.Core.Data;
using JobLink.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace JobLink.Api.ADO.Repositories
{
    public interface IInvoiceRepository :
        IAdaptor<System.Data.IDataReader, ICustomerInvoice>,
        System.IDisposable
    {
        ICustomerInvoice InsertInvoice(ICustomerInvoice customerInvoice);
        void CalculateTotal(Guid invoiceID);
        ICustomerInvoice GetInvoice(Guid invoiceID);
        ICollection<ICustomerInvoice> GetInvoices();
    }

    public class InvoiceRepository : RepositoryBase, IInvoiceRepository
    {
        public InvoiceRepository()
        { }


        public InvoiceRepository(IDatabase database)
            : base(database)
        { }

        public ICustomerInvoice Adapt(IDataReader datareader)
        {
            return new CustomerInvoice()
            {
                ID = (Guid)datareader["ID"],
                CustomerID = (Guid)datareader["CustomerID"],
                DocumentNumber = datareader["DocumentNumber"].ToString(),
                DateCreated = (DateTime)datareader["DateCreated"],
                Total = (decimal)datareader["Total"],
                IsDeleted = (bool)datareader["IsDeleted"],
                CustomerName = datareader["CustomerName"].ToString(),
            };
        }

        public ICustomerInvoice GetInvoice(Guid invoiceID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_CustomerInvoice_ID]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = invoiceID
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                {
                    return Read(datareader, this);
                }
            }
        }

        public ICollection<ICustomerInvoice> GetInvoices()
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_CustomerInvoices]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                {
                    return ReadCollection(datareader, this);
                }
            }
        }

        public ICustomerInvoice InsertInvoice(ICustomerInvoice customerInvoice)
        {
            Database.Validate();
            customerInvoice.ID = Guid.NewGuid();
            using (var command = new SqlCommand("[test].[USP_INSERT_CustomerInvoice]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoice.ID
                });
                command.Parameters.Add(new SqlParameter("@customerID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoice.CustomerID
                });
                command.Parameters.Add(new SqlParameter("@docNo", SqlDbType.NVarChar, 50)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoice.DocumentNumber
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                { }

                return customerInvoice;
            }
        }

        public void CalculateTotal(Guid invoiceID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_INSERT_CustomerInvoice]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = invoiceID
                });

                //run the command(query)
                using (SqlDataReader datareader = command.ExecuteReader())
                { }
            }
        }
    }
}
