﻿using JobLink.Core.Data;
using JobLink.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JobLink.Api.ADO.Repositories
{
    public interface IInvoiceRepositoryAsync : IInvoiceRepository
    {
        Task<ICustomerInvoice> InsertInvoiceAsync(ICustomerInvoice customerInvoice);
        Task CalculateTotalAsync(Guid invoiceID);
        Task<ICustomerInvoice> GetInvoiceAsync(Guid invoiceID);
        Task<ICollection<ICustomerInvoice>> GetInvoicesAsync();
    }

    public class InvoiceRepositoryAsync : InvoiceRepository, IInvoiceRepositoryAsync
    {
        public InvoiceRepositoryAsync()
        { }

        public InvoiceRepositoryAsync(IDatabase database)
            : base(database)
        { }

        public async Task<ICustomerInvoice> GetInvoiceAsync(Guid invoiceID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_CustomerInvoice_ID]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = invoiceID
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                {
                    return Read(datareader, this);
                }
            }
        }

        public async Task<ICollection<ICustomerInvoice>> GetInvoicesAsync()
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_GET_CustomerInvoices]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                {
                    return await ReadCollectionAsync(datareader, this);
                }
            }
        }

        public async Task<ICustomerInvoice> InsertInvoiceAsync(ICustomerInvoice customerInvoice)
        {
            Database.Validate();
            customerInvoice.ID = Guid.NewGuid();
            using (var command = new SqlCommand("[test].[USP_INSERT_CustomerInvoice]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoice.ID
                });
                command.Parameters.Add(new SqlParameter("@customerID", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoice.CustomerID
                });
                command.Parameters.Add(new SqlParameter("@docNo", SqlDbType.NVarChar, 50)
                {
                    Direction = ParameterDirection.Input,
                    Value = customerInvoice.DocumentNumber
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                { }

                return customerInvoice;
            }
        }

        public async Task CalculateTotalAsync(Guid invoiceID)
        {
            Database.Validate();
            using (var command = new SqlCommand("[test].[USP_UPDATE_CustomerInvoice]", Database.SqlConnection as SqlConnection))
            {
                //set commandType that i want to fire
                command.CommandType = CommandType.StoredProcedure;
                // input parameters that the stored proc will be expecting
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier)
                {
                    Direction = ParameterDirection.Input,
                    Value = invoiceID
                });

                //run the command(query)
                using (SqlDataReader datareader = await command.ExecuteReaderAsync())
                { }
            }
        }
    }
}
