﻿using JobLink.ADO.Repositories;
using JobLink.Api.ADO.Repositories;
using JobLink.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobLink.Api.Controllers
{
    [Route("api/inventory")]
    [ApiController]
    public class InventoryItemController : ControllerBase, IDisposable
    {
        //Please note that exception handling would have been updated with an application exception handler. 

        private readonly IInventoryItemRepositoryAsync inventoryItemRepository;

        public InventoryItemController(IInventoryItemRepositoryAsync inventoryItemRepository)
        {
            this.inventoryItemRepository = inventoryItemRepository;
        }

        public void Dispose()
        {
            inventoryItemRepository.Dispose();
        }

        [HttpGet]
        [Route("Items")]
        public async Task<ActionResult> GetInventoryItemsAsync()
        {
            try
            {
                var result = await inventoryItemRepository.GetInventoryItemsAsync();

                if (null != result)
                {
                    return Ok(result);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return BadRequest("Failed to get inventory items");
        }


        [HttpGet]
        [Route("Item")]
        public async Task<ActionResult> GetCustomerAsync(Guid itemID)
        {
            try
            {
                var result = await inventoryItemRepository.GetInventoryItemAsync(itemID);

                if (null != result)
                {
                    return Ok(result);
                }

                return BadRequest("Failed to get item");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        [Route("Insert")]
        public async Task<ActionResult> InsertAsync([FromBody] object body)
        {
            try
            {
                InventoryItem inventoryItem = JsonConvert.DeserializeObject<InventoryItem>(body.ToString());

                var result = await inventoryItemRepository.InsertInventoryItemAsync(inventoryItem);

                if (null != result)
                {
                    return Ok(result);
                }

                return BadRequest("Failed to insert item");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] object body)
        {
            InventoryItem inventoryItem = JsonConvert.DeserializeObject<InventoryItem>(body.ToString());

            var result = await inventoryItemRepository.UpdateInventoryItemAsync(inventoryItem);

            if (null != result)
            {
                return Ok(result);
            }

            return BadRequest("Failed to update inventory item");
        }
    }
}
