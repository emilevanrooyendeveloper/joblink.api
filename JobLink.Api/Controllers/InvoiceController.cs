﻿using JobLink.ADO.Repositories;
using JobLink.Api.ADO.Repositories;
using JobLink.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobLink.Api.Controllers
{
    [Route("api/invoice")]
    [ApiController]
    public class InvoiceController : ControllerBase, IDisposable
    {
        //Please note that exception handling would have been updated with an application exception handler. 

        private readonly IInvoiceRepositoryAsync invoiceRepository;

        public InvoiceController(IInvoiceRepositoryAsync invoiceRepository)
        {
            this.invoiceRepository = invoiceRepository;
        }

        public void Dispose()
        {
            invoiceRepository.Dispose();
        }

        [HttpGet]
        [Route("Invoices")]
        public async Task<ActionResult> GetInvoicesAsync()
        {
            try
            {
                var result = await invoiceRepository.GetInvoicesAsync();

                if (null != result)
                {
                    return Ok(result);
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            return BadRequest("Failed to get invoices");
        }


        [HttpGet]
        [Route("Invoice")]
        public async Task<ActionResult> GetInvoice(Guid invoiceID)
        {
            try
            {
                var result = await invoiceRepository.GetInvoiceAsync(invoiceID);

                if (null != result)
                {
                    return Ok(result);
                }

                return BadRequest("Failed to get invoices");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        [Route("Insert")]
        public async Task<ActionResult> InsertAsync([FromBody] object body)
        {
            try
            {

                CustomerInvoice invoice = JsonConvert.DeserializeObject<CustomerInvoice>(body.ToString());

                var result = await invoiceRepository.InsertInvoiceAsync(invoice);

                if (null != result)
                {
                    return Ok(result);
                }

                return BadRequest("Failed to insert invoice");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        [Route("Calculate")]
        public async Task<ActionResult> UpdateAsync(Guid invoiceID)
        {
            await invoiceRepository.CalculateTotalAsync(invoiceID);
            var result = await invoiceRepository.GetInvoiceAsync(invoiceID);

            if (null != result)
            {
                return Ok(result);
            }

            return BadRequest("Failed to calculate");
        }
    }
}
