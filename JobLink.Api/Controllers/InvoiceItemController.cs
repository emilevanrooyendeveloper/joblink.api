﻿using JobLink.ADO.Repositories;
using JobLink.Api.ADO.Repositories;
using JobLink.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobLink.Api.Controllers
{
    [Route("api/invoiceitem")]
    [ApiController]
    public class InvoiceItemController : ControllerBase, IDisposable
    {
        //Please note that exception handling would have been updated with an application exception handler. 

        private readonly IInvoiceItemRepositoryAsync invoiceItemRepository;

        public InvoiceItemController(IInvoiceItemRepositoryAsync invoiceItemRepository)
        {
            this.invoiceItemRepository = invoiceItemRepository;
        }

        public void Dispose()
        {
            invoiceItemRepository.Dispose();
        }

        [HttpGet]
        [Route("Items")]
        public async Task<ActionResult> GetInvoiceItems(Guid invoiceID)
        {
            try
            {
                var result = await invoiceItemRepository.GetCustomerInvoiceItemsAsync(invoiceID);

                if (null != result)
                {
                    return Ok(result);
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            return BadRequest("Failed to get invoice items");
        }


        [HttpGet]
        [Route("Remove")]
        public async Task<ActionResult> Remove(Guid itemID)
        {
            await invoiceItemRepository.RemoveAsync(itemID);

            return Ok();
        }

        [HttpPost]
        [Route("Insert")]
        public async Task<ActionResult> InsertAsync([FromBody] object body)
        {
            try
            {
                CustomerInvoiceItem invoice = JsonConvert.DeserializeObject<CustomerInvoiceItem>(body.ToString());

                var result = await invoiceItemRepository.InsertAsync(invoice);

                if (null != result)
                {
                    return Ok(result);
                }

                return BadRequest("Failed to insert invoice item");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
