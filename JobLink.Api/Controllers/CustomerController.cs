﻿using JobLink.ADO.Repositories;
using JobLink.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobLink.Api.Controllers
{
    [Route("api/customer")]
    [ApiController]
    public class CustomerController : ControllerBase, IDisposable
    {
        //Please note that exception handling would have been updated with an application exception handler. 

        private readonly ICustomerRepositoryAsync customerRepository;

        public CustomerController(ICustomerRepositoryAsync customerRepository)
        {
            this.customerRepository = customerRepository;
        }

        public void Dispose()
        {
            customerRepository.Dispose();
        }

        [HttpGet]
        [Route("Customers")]
        public async Task<ActionResult> GetCustomersAsync()
        {
            try
            {
                var result = await customerRepository.GetCustomersAsync();

                if (null != result)
                {
                    return Ok(result);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return BadRequest("Failed to get customers");
        }


        [HttpGet]
        [Route("Customer")]
        public async Task<ActionResult> GetCustomerAsync(Guid customerID)
        {
            try
            {
                var result = await customerRepository.GetCustomerAsync(customerID);

                if (null != result)
                {
                    return Ok(result);
                }

                return BadRequest("Failed to get customer");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        [Route("Insert")]
        public async Task<ActionResult> InsertAsync([FromBody] object body)
        {
            try
            {

                Customer customer = JsonConvert.DeserializeObject<Customer>(body.ToString());

                var result = await customerRepository.InsertCustomerAsync(customer);

                if (null != result)
                {
                    return Ok(result);
                }

                return BadRequest("Failed to insert customer");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] object body)
        {
            Customer customer = JsonConvert.DeserializeObject<Customer>(body.ToString());

            var result = await customerRepository.UpdateCustomerAsync(customer);

            if (null != result)
            {
                return Ok(result);
            }

            return BadRequest("Failed to update customer");
        }
    }
}
