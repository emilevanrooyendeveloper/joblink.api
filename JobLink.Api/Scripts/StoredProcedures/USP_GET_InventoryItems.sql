﻿CREATE PROCEDURE [test].[USP_GET_InventoryItems]
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[test].InventoryItems AS ii WITH(NOLOCK)
	WHERE
		IsDeleted = 0
END

