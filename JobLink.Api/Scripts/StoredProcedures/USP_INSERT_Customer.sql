﻿CREATE PROCEDURE [test].[USP_INSERT_Customer]
	@id UNIQUEIDENTIFIER,
	@customerName NVARCHAR(MAX), 
	@customerAddress NVARCHAR(MAX), 
	@telephoneNumber NVARCHAR(15)
AS 
BEGIN 
	INSERT INTO Customers(ID, CustomerName, [Address], TelephoneNumber, IsDeleted)
	VALUES(@id, @customerName, @customerAddress, @telephoneNumber, 0)
END
