﻿CREATE PROCEDURE [test].[USP_GET_InvoiceItems_InvoiceID]
	@invoiceID UNIQUEIDENTIFIER
AS 
BEGIN 
	SELECT 
		cii.*,
		ii.ItemDescription,
		ii.UnitPrice
	FROM 
		[test].CustomerInvoices AS ci WITH(NOLOCK) 
		INNER JOIN [test].CustomerInvoiceItems AS cii WITH(NOLOCK) ON cii.InvoiceID = ci.ID
		INNER JOIN [test].InventoryItems AS ii WITH(NOLOCK) ON ii.ID = cii.InventoryItemID
	WHERE 
		ci.ID = @invoiceID
		AND cii.IsDeleted = 0
END
