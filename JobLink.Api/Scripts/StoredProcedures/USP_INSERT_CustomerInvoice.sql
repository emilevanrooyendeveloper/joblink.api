﻿CREATE PROCEDURE [test].[USP_INSERT_CustomerInvoice]
	@id UNIQUEIDENTIFIER, 
	@customerID UNIQUEIDENTIFIER, 
	@docNo NVARCHAR(50)
AS 
BEGIN 
	INSERT INTO [test].CustomerInvoices(ID, CustomerID, DocumentNumber, Total, DateCreated, IsDeleted)
	VALUES(@id, @customerID, @docNo, 0, GETDATE(), 0)
END

