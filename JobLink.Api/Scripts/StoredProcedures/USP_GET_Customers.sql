﻿CREATE PROCEDURE [test].[USP_GET_Customers]
AS 
BEGIN 
	SELECT 
		* 
	FROM 
		[test].Customers AS c WITH(NOLOCK)
	WHERE 
		c.IsDeleted = 0
END
