﻿ALTER PROCEDURE [test].[USP_UPDATE_CustomerInvoice]
	@id UNIQUEIDENTIFIER
AS 
BEGIN 
	UPDATE [test].CustomerInvoices
	SET
		Total = 
			(SELECT 
				SUM(coi.Quantity * ii.UnitPrice) AS 'InvoiceTotal'
			FROM 
				[test].CustomerInvoiceItems AS coi 
				INNER JOIN [test].InventoryItems AS ii WITH(NOLOCK) ON ii.ID = coi.InventoryItemID
			WHERE 
				coi.IsDeleted = 0
				AND ii.IsDeleted = 0
				AND coi.InvoiceID = @id)
	WHERE
		ID = @id
END
