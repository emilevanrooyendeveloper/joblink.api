﻿ALTER PROCEDURE [test].[USP_GET_CustomerInvoice_ID]
	@id UNIQUEIDENTIFIER
AS 
BEGIN 
	SELECT TOP(1)
		ci.*,
		c.CustomerName
	FROM 
		[test].CustomerInvoices  AS ci WITH(NOLOCK) 
		INNER JOIN [test].Customers AS c WITH(NOLOCK) ON c.ID = ci.CustomerID
	WHERE 
		ci.ID = @id
END

