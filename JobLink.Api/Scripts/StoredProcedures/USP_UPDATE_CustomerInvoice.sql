﻿ALTER PROCEDURE [test].[USP_UPDATE_CustomerInvoice]
	@id UNIQUEIDENTIFIER
AS 
BEGIN 

(SELECT 
	SUM(coi.Quantity * ii.UnitPrice) AS 'Total'
FROM 
	[test].CustomerInvoiceItems AS coi 
	INNER JOIN [test].InventoryItems AS ii WITH(NOLOCK) ON ii.ID = coi.InventoryItemID
WHERE 
	coi.IsDeleted = 0
	AND ii.IsDeleted = 0
	AND coi.InvoiceID = @id)

	UPDATE [test].CustomerInvoices
	SET
		Total = COALESCE(Total, 0)
	WHERE
		ID = @id
END

