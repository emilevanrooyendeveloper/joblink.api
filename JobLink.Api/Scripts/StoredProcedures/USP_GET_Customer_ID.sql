﻿CREATE PROCEDURE [test].[USP_GET_Customer_ID]
	@customerID UNIQUEIDENTIFIER
AS 
BEGIN 
	SELECT TOP(1) 
		* 
	FROM 
		[test].Customers AS c WITH(NOLOCK)
	WHERE 
		c.ID = @customerID
END
