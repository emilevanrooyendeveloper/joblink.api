﻿CREATE PROCEDURE [test].[USP_UPDATE_InventoryItem]
	@id UNIQUEIDENTIFIER,
	@itemDescription NVARCHAR(MAX), 
	@unitPrice DECIMAL
AS 
BEGIN 
	UPDATE [test].InventoryItems 
	SET
		ItemDescription = @itemDescription,
		UnitPrice = @unitPrice
	WHERE
		ID = @id
END

