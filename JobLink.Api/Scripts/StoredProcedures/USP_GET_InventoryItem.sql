﻿CREATE PROCEDURE [test].[USP_GET_InventoryItem]
	@id UNIQUEIDENTIFIER
AS 
BEGIN 
	SELECT TOP(1)
		*
	FROM 
		[test].InventoryItems AS ii WITH(NOLOCK)
	WHERE
		ID = @id
END

