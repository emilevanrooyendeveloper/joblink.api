﻿CREATE PROCEDURE [test].[USP_UPDATE_Customer]
	@id UNIQUEIDENTIFIER,
	@customerName NVARCHAR(MAX), 
	@customerAddress NVARCHAR(MAX), 
	@telephoneNumber NVARCHAR(15), 
	@isDeleted BIT
AS 
BEGIN 
	UPDATE 
		Customers
	SET 
		CustomerName = @customerName,
		[Address] = @customerAddress,
		TelephoneNumber = @telephoneNumber,
		IsDeleted = @isDeleted
	WHERE
		ID = @id
END
