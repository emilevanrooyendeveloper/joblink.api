﻿CREATE PROCEDURE [test].[USP_INSERT_InventoryItem]
	@id UNIQUEIDENTIFIER,
	@itemDescription NVARCHAR(MAX), 
	@unitPrice DECIMAL
AS 
BEGIN 
	INSERT INTO
		[test].InventoryItems (ID, ItemDescription, UnitPrice, IsDeleted)
	VALUES(@id, @itemDescription, @unitPrice, 0)
END

