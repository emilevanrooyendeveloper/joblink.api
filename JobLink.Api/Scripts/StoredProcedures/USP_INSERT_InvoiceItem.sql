﻿ALTER PROCEDURE [test].[USP_INSERT_InvoiceItem]
	@id UNIQUEIDENTIFIER,
	@invoiceID UNIQUEIDENTIFIER,
	@quantity INT, 
	@inventoryItemID UNIQUEIDENTIFIER
AS 
BEGIN 
	IF EXISTS (SELECT 1 
			   FROM test.CustomerInvoiceItems
			   WHERE InvoiceID = @invoiceID 
			   AND InventoryItemID = @inventoryItemID)
	 BEGIN 
		-- if the id exist update
		UPDATE 
			test.CustomerInvoiceItems
		SET Quantity = (SELECT TOP(1) Quantity
						FROM test.CustomerInvoiceItems
						 WHERE InvoiceID = @invoiceID 
						AND InventoryItemID = @inventoryItemID) + @quantity
		WHERE 
			InvoiceID = @invoiceID 
			AND InventoryItemID = @inventoryItemID
	 END   
	ELSE
	BEGIN 
		INSERT INTO [test].CustomerInvoiceItems(ID, InvoiceID, InventoryItemID, Quantity, IsDeleted)
		VALUES(@id, @invoiceID, @inventoryItemID, @quantity, 0)
	END
END

