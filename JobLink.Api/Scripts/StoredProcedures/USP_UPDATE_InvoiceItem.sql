﻿CREATE PROCEDURE [test].[USP_UPDATE_InvoiceItem]
	@id UNIQUEIDENTIFIER
AS 
BEGIN 
	UPDATE [test].CustomerInvoiceItems
	SET
		IsDeleted = 1
	WHERE
		ID = @id
END

