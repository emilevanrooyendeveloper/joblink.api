﻿/****** Object:  Table [test].[InventoryItems]    Script Date: 10/05/2021 02:11:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [test].[InventoryItems](
	[ID] [uniqueidentifier] NOT NULL,
	[ItemDescription] [nvarchar](max) NULL,
	[UnitPrice] [decimal](18, 2) NOT NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_InventoryItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


