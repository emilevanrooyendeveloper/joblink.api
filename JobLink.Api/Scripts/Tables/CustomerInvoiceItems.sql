﻿
/****** Object:  Table [test].[CustomerInvoiceItems]    Script Date: 10/05/2021 02:10:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [test].[CustomerInvoiceItems](
	[ID] [uniqueidentifier] NOT NULL,
	[InvoiceID] [uniqueidentifier] NOT NULL,
	[InventoryItemID] [uniqueidentifier] NOT NULL,
	[Quantity] [int] NOT NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_CustomerInvoiceItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [test].[CustomerInvoiceItems]  WITH CHECK ADD  CONSTRAINT [FK_CustomerInvoiceItems_InventoryItem] FOREIGN KEY([InventoryItemID])
REFERENCES [test].[InventoryItems] ([ID])
GO

ALTER TABLE [test].[CustomerInvoiceItems] CHECK CONSTRAINT [FK_CustomerInvoiceItems_InventoryItem]
GO

ALTER TABLE [test].[CustomerInvoiceItems]  WITH CHECK ADD  CONSTRAINT [FK_CustomerInvoiceItems_Invoice] FOREIGN KEY([InvoiceID])
REFERENCES [test].[CustomerInvoices] ([ID])
GO

ALTER TABLE [test].[CustomerInvoiceItems] CHECK CONSTRAINT [FK_CustomerInvoiceItems_Invoice]
GO


