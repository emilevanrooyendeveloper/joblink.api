﻿/****** Object:  Table [test].[CustomerInvoices]    Script Date: 10/05/2021 02:10:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [test].[CustomerInvoices](
	[ID] [uniqueidentifier] NOT NULL,
	[CustomerID] [uniqueidentifier] NOT NULL,
	[DocumentNumber] [nvarchar](50) NOT NULL,
	[Total] [decimal](18, 2) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_CustomerInvoices] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [test].[CustomerInvoices]  WITH CHECK ADD  CONSTRAINT [FK_CustomerInvoices_CustomerID] FOREIGN KEY([CustomerID])
REFERENCES [test].[Customers] ([ID])
GO

ALTER TABLE [test].[CustomerInvoices] CHECK CONSTRAINT [FK_CustomerInvoices_CustomerID]
GO

