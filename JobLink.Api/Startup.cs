using JobLink.ADO.Repositories;
using JobLink.Api.ADO.Repositories;
using JobLink.Core.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobLink.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCors();

            services.AddControllers();
            Program program = new Program();

            services.AddTransient<IDatabase, Database>(i => new Database(Environment.GetEnvironmentVariable("DB_CONNECTION_STRING")));

            services.AddTransient<ICustomerRepositoryAsync, CustomerRepositoryAsync>();
            services.AddTransient<IInventoryItemRepositoryAsync, InventoryItemRepositoryAsync>();
            services.AddTransient<IInvoiceRepositoryAsync, InvoiceRepositoryAsync>();
            services.AddTransient<IInvoiceItemRepositoryAsync, InvoiceItemRepositoryAsync>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //Change database connection string here
                Environment.SetEnvironmentVariable("DB_CONNECTION_STRING", "Data Source=CORHQEMILEVR\\SQLSERVER2016DEV;Initial Catalog=_YOUR_DATABASENAME_;User Id=_YOUR_USERNAME_;Password=_Your_PASSWORD_; Integrated Security=SSPI;");

                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(options => options.AllowAnyOrigin()
                                          .AllowAnyHeader()
                                          .AllowAnyMethod());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
