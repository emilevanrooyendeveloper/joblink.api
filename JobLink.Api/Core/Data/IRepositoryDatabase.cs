﻿using System;

namespace JobLink.Core.Data
{
    public interface IRepositoryDatabase : IDisposable
    {
        IDatabase Database { get; set; }
    }
}
