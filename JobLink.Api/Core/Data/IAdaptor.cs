﻿namespace JobLink.Core.Data
{
    public interface IAdaptor<InputType, OutputType>
    {
        OutputType Adapt(InputType input); //Generic for adapter
    }
}
