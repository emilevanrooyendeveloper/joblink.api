﻿using System;

namespace JobLink.Core.Models
{
    public interface ICustomerInvoiceItem
    {
        Guid ID { get; set; }
        /// <summary>
        /// F.K. Reference to Customer Invoice ID
        /// </summary>
        Guid InvoiceID { get; set; }
        /// <summary>
        /// F.K. Reference to Inventory Item ID
        /// </summary>
        Guid InventoryItemID { get; set; }
        int Quantity { get; set; }
        bool IsDeleted { get; set; }

        string ItemDescription { get; set; }
        decimal UnitPrice { get; set; }
    }

    public class CustomerInvoiceItem: ICustomerInvoiceItem
    {
        public Guid ID { get; set; }
        /// <summary>
        /// F.K. Reference to Customer Invoice ID
        /// </summary>
        public Guid InvoiceID { get; set; }
        /// <summary>
        /// F.K. Reference to Inventory Item ID
        /// </summary>
        public Guid InventoryItemID { get; set; }
        public int Quantity { get; set; }
        public bool IsDeleted { get; set; }

        public string ItemDescription { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
