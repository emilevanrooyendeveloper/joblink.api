﻿using System;

namespace JobLink.Core.Models
{
    public interface ICustomer
    {
        Guid ID { get; set; }
        string CustomerName { get; set; }
        string Address { get; set; }
        string TelephoneNumber { get; set; }
        bool IsDeleted { get; set; }
    }

    public class Customer : ICustomer
    {
        public Guid ID { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string TelephoneNumber { get; set; }
        public bool IsDeleted { get; set; }
    }
}
