﻿using System;

namespace JobLink.Core.Models
{
    public interface ICustomerInvoice
    {
        Guid ID { get; set; }

        /// <summary>
        /// F.K. Reference to Customer ID
        /// </summary>
        Guid CustomerID { get; set; }
        string DocumentNumber { get; set; }
        decimal Total { get; set; }
        DateTime DateCreated { get; set; }
        bool IsDeleted { get; set; }

        string CustomerName { get; set; }
    }

    public class CustomerInvoice : ICustomerInvoice
    {
        public Guid ID { get; set; }

        /// <summary>
        /// F.K. Reference to Customer ID
        /// </summary>
        public Guid CustomerID { get; set; }
        public string DocumentNumber { get; set; }
        public decimal Total { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDeleted { get; set; }
        
        public string CustomerName { get; set; }
    }
}
