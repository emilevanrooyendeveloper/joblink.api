﻿using System;

namespace JobLink.Core.Models
{
    public interface IInventoryItem
    {
        Guid ID { get; set; }
        string ItemDescription { get; set; }
        decimal UnitPrice { get; set; }
        bool IsDeleted { get; set; }
    }

    public class InventoryItem : IInventoryItem
    {
        public Guid ID { get; set; }
        public string ItemDescription { get; set; }
        public decimal UnitPrice { get; set; }
        public bool IsDeleted { get; set; }
    }
}
